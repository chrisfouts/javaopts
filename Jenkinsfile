#!goovy
import groovy.json.*

private String secretsMapToJVMArgs(Map jsonJVMArgs) {
    def jvmArgs = ""
    jsonJVMArgs.collect { key, value ->
        jvmArgs += "-D${key}=${value} "
    }
    return jvmArgs
}

String fetchJVMArgs(String serviceName, String environment) {
    def secretsMap = new TreeMap()

    println "Processing secrets for service name: $serviceName in $environment"
    processSecretsForKey("cmm-mono", secretsMap)
    processSecretsForKey(serviceName, secretsMap)
    processSecretsForKey(environment, secretsMap)
    processSecretsForKey("${environment}-cmm-mono", secretsMap)
    processSecretsForKey("${environment}-${serviceName}", secretsMap)
    if (secretsMap) {
        // containers are listening on 8080
        secretsMap.put("local.server.port", "8080")
        secretsMap.put("local.management.port", "8080")
        secretsMap.put("server.port", "8080")
        secretsMap.put("management.port", "8080")
        return secretsMapToJVMArgs(secretsMap)
    } else error "No secrets found for service $serviceName OR environment $environment."

}

private void processSecretsForKey(String secretsKey, Map secretsMap) {
    try {
        secretsQuery = sh(
                script: "aws secretsmanager get-secret-value --secret-id ${secretsKey}",
                returnStdout: true
        )
        secretResults = readJSON text: secretsQuery

        if (secretResults.SecretString) {
            def jsonJVMArgs = readJSON text: secretResults.SecretString
            jsonJVMArgs.collect { key, value ->
                secretsMap.put(key, value)
            }
        }
    } catch (Exception e) {
        echo "Nothing found for service specific secrets (${secretsKey})"
    }
}


List SERVICES = [
  'zift-marcom',
  'zift-proc-proc',
  'zift-marcom-rest',
  'zift-admin-root',
  'zift-proc-root',
  'zift-proc-panel-pub',
  'zift-saml',
  'zift-proc-dm',
  'zift-zift-rest',
  'zift-proc-emergency',
  'zift-proc-dm-high',
  'zift-proc-mod',
  'zift-proc-content',
  'zift-admin',
  'zift-proc-proc-high',
  'zift-proc-infrastructure'
]
List ENVIRONMENTS = ['sandbox', 'staging', 'production']

node('linux') {
  stage('Get JavaOpts') {
    try {
      sh "aws --version"

      List Environments
      List Services
      if (env.ENVIRONMENT?.trim() == 'all') {
        Environments = ENVIRONMENTS
      }
      else {
        Environments = env.ENVIRONMENT.trim().split()
      }

      if (env.SERVICE?.trim()) {
        Services = env.SERVICE.trim().split()
      }
      else {
        Services = SERVICES
      }

      withAWS() {
        for (e in Environments) {
          for (s in Services) {
            jvmArgs = fetchJVMArgs(s, e)
            if (jvmArgs.length() >= 4096) {
              println "******************************************************"
              println "Env: $e, Service: $s, Length: "+jvmArgs.length().toString()
              println "******************************************************"
            }
          }
        }
      }
    }
    catch (org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException e) {
      throw e
    }
  }
}
